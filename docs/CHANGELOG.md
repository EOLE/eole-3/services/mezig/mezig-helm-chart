# Changelog

## [1.6.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.5.1...release/1.6.0) (2025-03-03)


### Features

* update appVersion to 1.13.1 ([a0443e6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/a0443e69a001a0d99bc5b84d988b29e454dcc69e))

### [1.5.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.5.0...release/1.5.1) (2024-10-08)


### Bug Fixes

* deploy appVersion to 1.13.0 ([bb6b3b9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/bb6b3b9cdac7fb75f6f782aaebd25ea5e5c79d20))

## [1.5.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.4.0...release/1.5.0) (2024-06-12)


### Features

* update helm chart appVersion to 1.12.0 ([2175ae0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/2175ae04dc4af5b5d6001c7a64837437a4631b87))


### Bug Fixes

* helm dev version deploy dev image tag ([66039bb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/66039bb4fbf6314a762e99be3ea27a4cf92ce0f2))
* restore testing tag ([3cf5bb2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/3cf5bb220b1dfeee39615a68e5e3f5110ac19cda))
* wait 5 minutes before killing pod ([c2fb8ab](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/c2fb8ab03c07d1b6303fc0471eaaf29677b00bcf))

## [1.4.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.3.0...release/1.4.0) (2024-01-16)


### Features

* update app version to 1.11.0 ([b7a989d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/b7a989d637bb8691017da48fe35ecbb93873f8ba))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.2.0...release/1.3.0) (2023-10-17)


### Features

* new stable helm for app version 1.10.0 ([655ad83](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/655ad839c452b9f4c581a80e241d7d2de6ff9733))


### Bug Fixes

* appversion reference testing image ([7a28036](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/7a28036298e685e3598822fbcd3954fb25dbd235))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.1.1...release/1.2.0) (2023-08-25)


### Features

* new stable version for appversion 1.9.0 ([cb3bc4d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/cb3bc4daf62947784ef49ed70025a325d937a0e2))

### [1.1.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.1.0...release/1.1.1) (2023-05-09)


### Bug Fixes

* add more description in readme ([decffb8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/decffb8166412c9eb751604223542b74812ea511))
* dev chart version deploy dev app version ([3436def](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/3436def63053c53eae2bdc1b14c4cc9eb0a9f2c6))
* helm testing deploy testing app version ([c8a6a6b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/c8a6a6b9fb00e281610a5e0581789dab28916696))
* publish new helm chart version ([2932df4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/2932df4d734d885aa570d02097970856dd66a6fd))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/compare/release/1.0.1...release/1.1.0) (2023-04-18)


### Features

* publish stable version 1.8.0 for mezig ([7e74852](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/mezig-helm-chart/commit/7e748527345704ab541a3488745e488f779886f7))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/compare/release/1.0.0...release/1.0.1) (2023-02-01)


### Bug Fixes

* publish mezig 1.7.1 ([da48a75](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/da48a75c50d1c0b387d872800d85136f584f552a))

## 1.0.0 (2022-12-09)


### Features

* **ci:** build helm package ([5ad7695](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/5ad769528e3be56a8a178b507772b11e73f8148f))
* **ci:** push helm package to chart repository ([7c502bc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/7c502bc0e0dc4017d7afdd9214780fb82e62c447))
* **ci:** update helm chart version with `semantic-release` ([d13f68c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/d13f68c64cf05f08c83cef32333a850b203a4134))
* **ci:** validate helm chart at `lint` stage ([a291bee](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/a291bee6100d587e95607c00882763a634e3cbc0))
* use chart.yaml as default version ([928ee02](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/928ee022f516619b7d04e0737eec366603d5b125))


### Bug Fixes

* **mezig:** add containerport default value ([a478fbc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/a478fbc7637099592d498da3a2a21bacd3065e8b))
* **mezig:** add image repository default value ([0a49e5d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/0a49e5d27ad252519c290abc21814f568b0293e0))
* update hpa api version removed on k8s 1.26 ([dbe2fd4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/dbe2fd4cc0eddff19e424f11a86af788951438a5))


### Continuous Integration

* **commitlint:** enforce commit message format ([90ea9a3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/90ea9a32c0db64f47c38e1c3017132423d3be8fe))
* **release:** create release automatically with `semantic-release` ([8eb975a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mezig/commit/8eb975ab5fe750223a103c445bf13333459a6046))
